# Formation GIT

# Objectif - tester les commandes avancées de git :

- Le rebase et la gestion de conflits
- le reset de commit
- git stash
- le squash de commit avec le rebase -i

# Les tests à faire

Exercice 1 :

- Créer un commit et le défaire
- Recréer le commit, modifier le nom du commit
- Créer 3 petits commits à la suite. Les squasher
- Faire un rebase avec la dev, envoyer ce commit sur la dev.
- Basculer sur la dev, observer le git log
  A refaire l'exercice plusieurs fois pour tester le reword, squash dans les interactives rebase (regarder les options proposées)

Exercice 2 :
Faire 3 commits. Revenir sur ces 3 commits (reset)

Exercice 3 :

- Créer une nouvelle branche à partir de la dev et travailler sur le footer avec plusieurs commits
- Avancer la dev et créer un conflit en modifiant le footer
- Revenir sur la branche,
  Répéter cet exercice plusieurs fois pour pratiquer la gestion de conflit
